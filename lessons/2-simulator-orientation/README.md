# Challenge 2: Device/Simulator Orientation

## Your Objective

The goal of this challenge it to become comfortable with light debugging and the basic React Native dev tools. You'll debug some errors, and then spend some time poking around the dev tools.

## Setup

Before you begin, replace the contents of your project's `App.js` file with the following:

```js
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

export default class App extends React.Component {
  render() {
    imNotDefined;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Nice work!</Text>
        <Image
          style={styles.image}
          source={{
            uri:
              "https://s3.amazonaws.com/vigesharing-is-vigecaring/lkurtz/rn-workshop-thumbs-up.gif"
          }}
        />
        <Text style={styles.instructions}>
          Now take a look around a bit with the dev tools.
        </Text>
        <Text style={styles.instructions}>
          Remember: press Cmd+D or shake for dev menu
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    imNotAProperty: 42
  },
  image: {
    height: 100,
    margin: 25,
    width: 200
  }
});
```

Save the file, then run the simulator again.

## Pointers

### Dealing With Errors

Once you complete the setup instructions, you might be thinking "...what the heck?"

![uh oh, error time](https://s3.amazonaws.com/vigesharing-is-vigecaring/lkurtz/rnwksp-c2-error.png)

This is the exception screen. If you're like me, you'll come to know this screen quite well.

You've already noticed that the screen contains a helpful error message. It also includes a stack trace of the exception underneath the error. Since this is an iPhone simulator, you can't scroll with a mouse wheel. Instead use your cursor as a finger. Depress the mouse button to touch your virtual finger to the screen.

Do you see where the error is coming from? The majority of stack traces will reference files in the React Native core. Let's focus on the files we control. What file in the trace looks familiar? Track down and destroy the bug, then click "Reload" to see if you got it.

Repeat until the file is error-free.

## Exploring Sim Functions

If you're using the iOS simulator, hit Cmd+Shift+H.

Nice, you just hit the phone's home button. Yep, the simulator is simulating an entire phone, not just your application. You can perform simulated hardware actions (like hitting the home button, locking, etc.) via the Hardware item in the Simulator's menubar. Check them out.

## Exploring the Dev Menu

React Native ships will a set of useful dev tools. You can take a look at these tools by hitting Cmd+D if you are using the iOS simulator or shaking comically hard if you're using an actual device. Explore around and get a sense for what each of these tools provides.

## Get to it.

Debug the app. Play with the simulator or device a bit. Expore the React Native dev tools.

Then, with Live Reload enabled, change some content in the components within `App.js`.

When you see your updated content in the simulator, post that you've completed Challenge 2 in the [#progress channel]([#progress channel](https://sxsw-rn.slack.com/messages/C9G1X11T2/), then proceed to Challenge 3.

---

[Proceed to Challenge 3](../3-straight-styling/index.html)
