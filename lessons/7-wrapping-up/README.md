# Wrapping Up

We hope you enjoyed these exercises, and welcome any feedback to make them better. Drop us a line on Slack with any thoughts.

Keep us posted about what you're building with React Native, and we'll be sure to brag about you during our next workshop. :-)

And if you're ever looking to put your new skills to work, [Viget](https://www.viget.com/careers) is always looking for smart people to work on stuff just like this. Tell them Lawson and Nate sent you ;-)
