# Challenge 4: Rockets, Rockets, Rockets

Stateless apps are boring. Let's make an app that actually does _something_.

## Your Objective

Make an app with a button that fires a rocket. The button should have some sort of safety to prevent accidental launches. For the purposes of this challenge, let's not actually fire rockets though. You can show an image of a launch instead.

Below is one example of what the app could look like, but the functionality and style of your own app is completely up to you!

<img title="one" src="https://monosnap.com/file/1GbLxIHl7VhNmycKHC4PoRkhlg9bDX.png" style="width: 100%" />
<img title="two" src="https://monosnap.com/file/ZkL9jC2CyMBsTVOZFxAVAiAEbqG9X9.png" style="width: 100%" />
<img title="three" src="https://monosnap.com/file/mW4zUKHk1Zd5WliLOpy7OR20DxaO0v.png" style="width: 100%" />

Find your own launch image, or use the one included in the [challenge-4 workshop files](../../files/challenge_files/challenge-4.zip). (Or if you'd like to use a URI-based image, you may use: https://monosnap.com/file/OjpDgW8xX2CwYwakkCQWLJUdu6mQp1.png)

## Setup

Before you begin, replace the contents of your project's `App.js` file with the following:

```js
import React from "react";
import {
  Button,
  Dimensions,
  Image,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View
} from "react-native";

export default class App extends React.Component {
  constructor() {
    super();
    // You may want to put stuff here.
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Up next...</Text>
        <Text style={styles.instructions}>
          Build an app to launch a rocket.
        </Text>
        <Text style={styles.instructions}>
          It should include some type of launch safeguard{"\n"}
          to enable and disable a main Launch button.
        </Text>
        <Text style={styles.instructions}>
          For the purposes of this example, don't actually{"\n"}
          launch a rocket. Instead you can just show a picture{"\n"}
          of a rocket launch instead. You can find one at{"\n"}
          ./resources/images/launch.gif
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#eee",
    alignItems: "center",
    justifyContent: "center"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 20
  },
  instructions: {
    marginTop: 10,
    textAlign: "center"
  }
});
```

## Pointers

### Buttons & Touchables

React Native includes a number of component types specifically designed to handle touch interactions. These include:

* [`<Button>`](https://facebook.github.io/react-native/docs/button.html)
* [`<TouchableHighlight>`](https://facebook.github.io/react-native/docs/touchablehighlight.html#content)
* [`<TouchableOpacity>`](https://facebook.github.io/react-native/docs/touchableopacity.html#content)
* [`<TouchableWithoutFeedback>`](https://facebook.github.io/react-native/docs/touchablewithoutfeedback.html#content)

All these components take a `onPress` prop for an interaction even handler.

Touchables accept the following additional event handler props:

* `onLongPress`
* `onPressIn`
* `onPressOut`

There is some overlap with the utility of these different components. Try them all out and you'll develop a feel for which is most appropriate for a given circumstance.

### Native Elements

React Native includes many component types that are simple wrappers around native UI elements. Example of such include [`<Button>`](https://facebook.github.io/react-native/docs/button.html) and [`<Switch>`](https://facebook.github.io/react-native/docs/switch.html#content). It is often a good idea to use as many native elements as possible to keep your interface familiar and performant. (It's also just easier to use these great components out of the box than to build your own versions of the same thing.) The official documentation is the best place for information about how to use each of these components.

## Get to it.

Make an app. Launch the rockets. When you're done, post a screenshot in the [#progress channel](https://sxsw-rn.slack.com/messages/C9G1X11T2/), then move on to Challenge 5.

---

[Proceed to Challenge 5](../5-navigation/index.html)
