# Introducing React Native

Now that you're familiar with React, you're ready to dive into React Native. React Native is simply a library for building native user interfaces using the React components that you now know (and hopefully love).

## What to Expect

The challenges that follow are self-guided. We've provided just enough information to get you started in the right direction. They become more free-form (and interesting) as you go. How far you take them is completely up to you.

Work through the challenges at your own pace. You probably won't finish them all, and that's okay. Our goal is to build familiarity and comfort with using React Native, not to cram every bit of its knowledge into a 4 hour power session. After the workshop you should feel enabled to continue to your learning. If you'd like, you may continue with the remaining challenges after the workshop at your leisure. We'll keep our Slack going after the workshop so y'all have a place to find support.

## Where to Get Help

### The Official React Native Docs

React Native's official documentation is the canonical source of information about React Native's capabilities and usage. Getting comfortable with the docs now will pay dividends as you continue to work with the library future.

[https://facebook.github.io/react-native/docs/getting-started.html](https://facebook.github.io/react-native/docs/getting-started.html)

### Your Neighbors

We want this to be a collaborative, interactive workshop. Feel free to work with a partner, or reach out to those around you if you need some help. Don't be shy; we're all in this together.

### Slack

The [#help channel](https://sxsw-rn.slack.com/messages/C9HGB9M0W/) in our Slack exists to surface problems or questions as they arise. Feel free to respond to others' questions, or drop in your own. If you haven't signed up for our Slack yet, you can do so [here](https://sxsw-rn.herokuapp.com/).

### Lawson, Nate, & Eli

We're here to help! Come find us, and we'll talk things through.

## Questions

If you have any questions during these challenges, feel free to ask Lawson, Nate, or Eli, or drop your question into the [#help channel](https://sxsw-rn.slack.com/messages/C9HGB9M0W/).

Alrighty now. Let's get started.

---

[Take me to the first challenge!](../1-zero-to-hero/index.html)
