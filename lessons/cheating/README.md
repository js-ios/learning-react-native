# The Cheat Sheet

If you're hopelessly stuck, and need to peak at a solution for your challenge, you can find them below. But note that a certain degree of struggle is a necessary component of the learning process. Don't give up on yourself too quickly.

* [Challenge 3](https://gitlab.com/ltk/sxsw-rn-2018/blob/e610ca2cbd931aaed08373513f8a3c373410c491/App.js)
* [Challenge 4](https://gitlab.com/ltk/sxsw-rn-2018/blob/37408049650d4e24831291c1c4a9cf82b996ef0f/App.js)
* [Challenge 5](https://gitlab.com/ltk/sxsw-rn-2018/blob/4bc02bb63b00cc6e637a00536beb14ada6803f96/App.js)
* [Challenge 6](https://gitlab.com/ltk/sxsw-rn-2018/blob/2294ac650b9dc47224aa3793c449a42ba3c66b8f/App.js)

## Full Challenge Repositories

* [Challenges 1-6](https://gitlab.com/ltk/sxsw-rn-2018/)
