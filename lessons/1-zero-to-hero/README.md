# Challenge 1: Zero to Hero

## Your Objective

The goal of this challenge it to initialize your first React Native app, and get it running on a device using Expo.

## Setup

You should already have:

* A computer with Expo XDE installed
* An iOS or Android device with Expo app installed

Distributions of Expo XDE for Mac, Linux, and Windows are available on USB sticks. Ask us for one if you need it.

## Pointers

### Project Initialization

#### What to Do Today

Download and unzip [`challenge-1.zip`](../../files/challenge_files/challenge-1.zip) from the workshop files. `cd` into the unzipped directory (should be named `FirstApp`) and run `npm rebuild`.

#### What You Would Usually Do (But Not Today)

There are multiple ways to start new React Native projects, but the easiest is by using `create-react-native-app`.

```
npm install -g create-react-native-app
create-react-native-app YourNewProject
cd YourNewProject
npm start
```

This sets up everything you need to start development using Expo.

See https://facebook.github.io/react-native/docs/getting-started.html#quick-start for more information about starting new projects.

##### Why aren't we doing this today?

Running `create-react-native-app` will download the newest version of React Native and all of its dependencies... which is very many megabytes. If we tried to do this today, SXSW's routers would melt, and our `npm install`s would never finish. Sadtimes all around.

### Running the Project

Remember that running an in-development React Native project has two components: a packager and an iOS or Android runtime.

Expo XDE will manage the packager and automatic transfer of the bundled package to the runtime.

After you've unzipped `challenge-1.zip`, open Expo XDE, and open the unzipped directory as a project. After Expo has initialized a tunnel for package transfers, you can point your runtime to the tunnel.

1.  If you're on a Mac with Xcode installed: it's easiest and fasted to develop using the iOS simulator. Click the Device button in Expo XDE, and choose Open on iOS Simulator.

2.  If you're using an Android device: it's easiest and fastest to use to develop using your USB-tethered device. If you haven't done so already, enable USB Debugging on your Android device. (Go to Settings -> About Phone and locating the Build Number field, tap on the field 7 times, then go to Settings -> Developer options, and toggle on USB Debugging, then jump up and down twice and spin around. You're all set.) Plug in your device to your computer via USB. Click the Device button in Expo XDE, and choose Open on Android.

3.  If none of the above: Click the Share button in Expo XDE. When you see the QR code, open up the Expo app on your device, and tap Scan QR Code. Scan the code. Boom.

## Get to it.

Go get the project running on a device or simulator.

<img src="https://monosnap.com/file/VpdE71lEO3RHTUggoKouyCGp1VuinP.png" title="this screenshot" style="width:100%;border:1px solid black;" />

When you see the above screen, congrats. You're a hero. Post that you've completed Challenge 1 in the [#progress channel](https://sxsw-rn.slack.com/messages/C9G1X11T2/), then move on to Challenge 2.

---

[Proceed to Challenge 2](../2-simulator-orientation/index.html)
