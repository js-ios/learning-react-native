# Challenge 3: Straight Styling

## Your Objective

The goal with this challenge is to gain familiarity with React Native's style and layout capabilities. To do this, you'll try to build the following screen:

![Your Target](https://monosnap.com/file/PRQic7aie4TUrLxdgK6ydqm8xWTEAw.png)

## Setup

Before you begin, replace the contents of your project's `App.js` file with the following:

```js
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Next challenge!</Text>
        <Text style={styles.instructions}>
          Try to build a replica of the screen{"\n"}
          shown in the challenge directions.
        </Text>
        <Text style={styles.instructions}>(The one below.)</Text>
        <Image
          style={styles.targetImage}
          source={{
            uri: "https://monosnap.com/file/PRQic7aie4TUrLxdgK6ydqm8xWTEAw.png"
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#eee",
    alignItems: "center",
    justifyContent: "center"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    marginTop: 10,
    textAlign: "center"
  },
  targetImage: {
    width: 200,
    height: 400,
    marginTop: 25
  }
});
```

## Pointers

### Intro to Pseudo-CSS

React Native does not implement CSS, however it uses JavaScript-based tooling this is inspired by and feels very much like CSS in certain ways.

#### Styles are represented as JSON.

```css
.page {
  background-color: orange;
  height: 42px;
  border: 1px solid blue;
}
```

...would be...

```js
{
  page: {
    backgroundColor: 'orange',
    height: 42,
    borderColor: 'blue',
    borderWidth: 1
  }
}
```

#### Styles don't cascade.

```js
<View style={{ color: "orange" }}>
  <Text>I'm not orange.</Text>
</View>
```

But you can build up styles compositionally:

```js
<View>
  <Text style={[styles.defaultText, styles.orange, styles.centered]}>
    I'm orange and centered.
  </Text>
</View>;

// ...

const styles = StyleSheet.create({
  defaultText: {
    fontSize: 12
  },
  orange: {
    color: "orange"
  },
  centered: {
    textAlign: "center"
  }
});
```

#### Layout is flexbox-based.

You won't find any `float` in React Native styles. Flexbox is a more powerful and elegant alternative, but can have a bit of a learning curve. If you're new to Flexbox layouts, the [CSS-Tricks Flexbox Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) is a teriffic resource.

### Useful Documentation

* [Overall Style Docs](https://facebook.github.io/react-native/docs/style.html)
* [View Properties](https://facebook.github.io/react-native/docs/view-style-props.html)
* [Image Properties](https://facebook.github.io/react-native/docs/image.html#style)
* [Text Properties](https://facebook.github.io/react-native/docs/text.html#style)
* [Flex Properties](https://facebook.github.io/react-native/docs/flexbox.html#content)
* [Transform Properties](https://facebook.github.io/react-native/docs/transforms.html#content)

## Get to it.

Learning time is over. It's time to build. How close can you get to the screen below?

<img title="Your Target" src="https://monosnap.com/file/PRQic7aie4TUrLxdgK6ydqm8xWTEAw.png" style="width: 100%" />

You can find the same image included on the above screen in the [challenge-3 workshop files](../../files/challenge_files/challenge-3.zip).

And remember that [`<View>`](https://facebook.github.io/react-native/docs/view.html) is analogous to HTML's `<div>`, [`<Text>`](https://facebook.github.io/react-native/docs/text.html) is analogous to `<p>`, and [`<Image>`](https://facebook.github.io/react-native/docs/image.html) is analogous to `<img>`.

When you're satisfied, post a screenshot of your app in the [#progress channel](https://sxsw-rn.slack.com/messages/C9G1X11T2/), then move on to Challenge 4. (If this style challenge felt too easy for you, spend some more time going crazy, pushing the limits of React Native styling. Then post a screenshot of your crazy.)

## Extra Credit

<img title="ugh" src="https://monosnap.com/file/GjShKXd04x3hLr3JZNBUbWVD4rAXnq.png" style="width: 100%" />

If you're annoyed by this black status bar text on the dark grey background, check out the [StatusBar component](https://facebook.github.io/react-native/docs/statusbar.html) docs and change it to a lighter color.

---

[Proceed to Challenge 4](../4-rockets/index.html)
