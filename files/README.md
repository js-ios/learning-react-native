# Workshop Files

## Challenge Files
- [challenge-1.zip](challenge_files/challenge-1.zip)

## Dependencies
### XDE
_Spare our network bandwidth; Do not attempt to download XDE today if you don't have it._

Instead, if you don't already have XDE installed, you can find [Mac](dependencies/XDE/xde-2.22.1.dmg), [Linux](dependencies/XDE/xde-2.22.1-x86_64.AppImage), and [Windows](dependencies/XDE/xde-Setup-2.22.1.exe) versions here in the course files.

### Node Modules
Under normal network conditions, you would simply `yarn add` or `npm install` all of these modules. If the SXSW network cannot support this, you can download modules via links below, and unzip them directly in your project's `node_modules` directory.

- [react-navigation](dependencies/node_modules/react-navigation.zip)
- [hoist-non-react-statics](dependencies/node_modules/hoist-non-react-statics.zip)
- [clamp](dependencies/node_modules/clamp.zip)
- [path-to-regexp](dependencies/node_modules/path-to-regexp.zip)
- [react-native-drawer-layout-polyfill](dependencies/node_modules/react-native-drawer-layout-polyfill.zip)
- [react-native-safe-area-view](dependencies/node_modules/react-native-safe-area-view.zip)
- [react-native-tab-view](dependencies/node_modules/react-native-tab-view.zip)
