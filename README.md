# Learning React Native

Six challenges to get you started with React Native.

1. [Intro](lessons/0-intro/index.html)
2. [Challenge 1: Zero to Hero](lessons/1-zero-to-hero/index.html)
3. [Challenge 2: Simulator Orientation](lessons/2-simulator-orientation/index.html)
4. [Challenge 3: Straight Styling](lessons/3-straight-styling/index.html)
5. [Challenge 4: Rockets](lessons/4-rockets/index.html)
6. [Challenge 5: Navigation](lessons/5-navigation/index.html)
7. [Challenge 6: Location](lessons/6-location/index.html)
9. [Wrapping Up](lessons/7-wrapping-up/index.html)

## Resourcess
- [Workshop Files](files/index.html)
- [The Cheat Sheet](lessons/cheating/index.html)
